package com.spring.fujitsutrialtask.mapper;

import com.spring.fujitsutrialtask.entity.WeatherDataEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class WeatherDataMapperTest {
    private String xmlData;
    private final WeatherDataMapper weatherDataMapper = Mappers.getMapper(WeatherDataMapper.class);

    @Test
    void convertXmlToEntityValidXmlDocumentCase() throws Exception {
        xmlData =
                "<root>" +
                "<observations timestamp=\"1711915276\">" +
                "<station>" +
                "<name>Tallinn-Harku</name>" +
                "<wmocode>12345</wmocode>" +
                "<airtemperature>15.5</airtemperature>" +
                "<windspeed>10.0</windspeed>" +
                "<phenomenon>rain</phenomenon>" +
                "</station>" +
                "</observations>" +
                "</root>";
        Document mockDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new ByteArrayInputStream(xmlData.getBytes()));

        Iterable<WeatherDataEntity> entities = weatherDataMapper.convertXmlToEntity(mockDocument);

        List<WeatherDataEntity> entityList = List.copyOf((Collection<? extends WeatherDataEntity>) entities);
        assertEquals(1, entityList.size());
        WeatherDataEntity entity = entityList.get(0);
        assertEquals("Tallinn", entity.getStationName());
        assertEquals(12345, entity.getStationWmo());
        assertEquals(15.5f, entity.getAirTemperature());
        assertEquals(10.0f, entity.getWindSpeed());
        assertEquals("rain", entity.getPhenomenon());
        assertEquals(LocalDateTime.parse("2024-03-31T20:01:16"), entity.getTimestamp());
    }

    @Test
    void convertXmlToEntityXmlDocumentWithMissingTimestampCase() throws ParserConfigurationException, IOException, SAXException {
        xmlData =
                "<root>" +
                "<observations>" +
                "<station>" +
                "<name>Tallinn-Harku</name>" +
                "<wmocode>12345</wmocode>" +
                "<airtemperature>15.5</airtemperature>" +
                "<windspeed>10.0</windspeed>" +
                "<phenomenon>rain</phenomenon>" +
                "</station>" +
                "</observations></root>";
        Document mockDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new ByteArrayInputStream(xmlData.getBytes()));

        Iterable<WeatherDataEntity> entities = weatherDataMapper.convertXmlToEntity(mockDocument);

        List<WeatherDataEntity> entityList = List.copyOf((Collection<? extends WeatherDataEntity>) entities);
        assertEquals(1, entityList.size());
        WeatherDataEntity entity = entityList.get(0);
        assertNotNull(entity.getTimestamp());
    }

    @Test
    void convertXmlToEntityEmptyXmlDocumentCase() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document emptyDocument = db.newDocument();

        Iterable<WeatherDataEntity> entities = weatherDataMapper.convertXmlToEntity(emptyDocument);

        assertFalse(entities.iterator().hasNext());
    }

    @Test
    void convertXmlToEntityNullXmlDocumentCase() {
        Throwable exception = assertThrows(NullPointerException.class, () ->
                weatherDataMapper.convertXmlToEntity(null));

        String expectedMessage = "xml document was empty.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}