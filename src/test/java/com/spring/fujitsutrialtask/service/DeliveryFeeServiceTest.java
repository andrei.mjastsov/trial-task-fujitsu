package com.spring.fujitsutrialtask.service;

import com.spring.fujitsutrialtask.entity.WeatherDataEntity;
import com.spring.fujitsutrialtask.error.ServiceError;
import com.spring.fujitsutrialtask.repository.WeatherDataRepository;
import com.spring.fujitsutrialtask.utils.City;
import com.spring.fujitsutrialtask.utils.Vehicle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeliveryFeeServiceTest {

    @InjectMocks
    private DeliveryFeeService deliveryFeeService;
    @Mock
    private WeatherDataRepository weatherDataRepository;

    private WeatherDataEntity createWeatherDataEntity(Float airTemperature, Float windSpeed, String phenomenon) {
        WeatherDataEntity entity = new WeatherDataEntity();
        entity.setAirTemperature(airTemperature);
        entity.setWindSpeed(windSpeed);
        entity.setPhenomenon(phenomenon);
        return entity;
    }

    @Test
    void calculateDeliveryFeeValidWeatherDataWithTimestampProvidedCase() throws ServiceError {
        City city = City.TALLINN;
        Vehicle vehicleType = Vehicle.CAR;
        LocalDateTime dateTime = LocalDateTime.now();
        WeatherDataEntity weatherData = createWeatherDataEntity(20f, 15f, "Rain");
        when(weatherDataRepository.getByStationNameAndTimestamp(any(), any())).thenReturn(weatherData);

        double result = deliveryFeeService.calculateDeliveryFee(city, vehicleType, dateTime);

        double expected = 4.0;
        assertEquals(expected, result);
    }

    @Test
    void calculateDeliveryFeeValidWeatherDataWithoutTimestampCase() throws ServiceError {
        City city = City.TALLINN;
        Vehicle vehicleType = Vehicle.CAR;
        WeatherDataEntity weatherData = createWeatherDataEntity(20f, 15f, "sleet");
        when(weatherDataRepository.getByStationName(any())).thenReturn(weatherData);

        double result = deliveryFeeService.calculateDeliveryFee(city, vehicleType, null);

        double expected = 4.0;
        assertEquals(expected, result);
    }

    @Test
    void calculateDeliveryFeeMissingWeatherDataCase() {
        City city = City.TALLINN;
        Vehicle vehicleType = Vehicle.CAR;
        LocalDateTime dateTime = LocalDateTime.now();

        assertThrows(ServiceError.class, () -> deliveryFeeService.calculateDeliveryFee(city, vehicleType, dateTime));
    }

    @Test
    void calculateATEFCarVehicleCase() {
        WeatherDataEntity weatherData = createWeatherDataEntity(-10f, 15f, "");
        Vehicle vehicleType = Vehicle.CAR;

        double result = deliveryFeeService.calculateATEF(weatherData, vehicleType);

        assertEquals(0d, result);
    }

    @Test
    void calculateATEFBikeOrScooterAirTemperatureBelowMinus10Case() {
        WeatherDataEntity weatherData = createWeatherDataEntity(-15.0f, 15f, "");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateATEF(weatherData, vehicleType);

        assertEquals(1d, result);
    }

    @Test
    void calculateATEFBikeOrScooterAirTemperatureBetweenMinus10And0Case() {
        WeatherDataEntity weatherData = createWeatherDataEntity(-5.0f, 15f, "");
        Vehicle vehicleType = Vehicle.SCOOTER;

        double result = deliveryFeeService.calculateATEF(weatherData, vehicleType);

        assertEquals(0.5d, result);
    }

    @Test
    void calculateATEFBikeOrScooterAirTemperatureAbove0Case() {
        WeatherDataEntity weatherData = createWeatherDataEntity(5f, 15f, "");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateATEF(weatherData, vehicleType);

        assertEquals(0d, result);
    }

    @Test
    void calculateATEFBikeOrScooterAirTemperature0Case() {
        WeatherDataEntity weatherData = createWeatherDataEntity(0f, 15f, "");
        Vehicle vehicleType = Vehicle.SCOOTER;

        double result = deliveryFeeService.calculateATEF(weatherData, vehicleType);

        assertEquals(0.5d, result);
    }

    @Test
    void calculateWSEFBikeWindSpeedBelow10Case() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 8f, "");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateWSEF(weatherData, vehicleType);

        assertEquals(0d, result);

    }

    @Test
    void calculateWSEFBikeWindSpeedBetween10And20Case() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 15f, "");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateWSEF(weatherData, vehicleType);

        assertEquals(0.5d, result);
    }

    @Test
    void calculateWSEFBikeWindSpeedAbove20Case() {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 33.0f, "");
        Vehicle vehicleType = Vehicle.BIKE;

        ServiceError thrownException = assertThrows(ServiceError.class, () -> deliveryFeeService.calculateWSEF(weatherData, vehicleType));
        assertEquals("Usage of selected vehicle type is forbidden", thrownException.getMessage());
    }

    @Test
    void calculateWSEFNonBikeVehicleCase() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 33.0f, "");
        Vehicle vehicleType = Vehicle.SCOOTER;

        double result = deliveryFeeService.calculateWSEF(weatherData, vehicleType);

        assertEquals(0d, result);
    }

    @Test
    void calculateWPEFBikeOrScooterSnowPhenomenonCase() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(-12f, 19.5f,"snow");
        Vehicle vehicleType = Vehicle.SCOOTER;

        double result = deliveryFeeService.calculateWPEF(weatherData, vehicleType);

        assertEquals(1.0d, result);
    }

    @Test
    void calculateWPEFBikeOrScooterSleetPhenomenonCase() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(-12f, 19.5f,"sleet");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateWPEF(weatherData, vehicleType);

        assertEquals(1.0d, result);
    }

    @Test
    void calculateWPEFBikeOrScooterRainPhenomenonCase() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(5f, 12.5f,"rain");
        Vehicle vehicleType = Vehicle.BIKE;

        double result = deliveryFeeService.calculateWPEF(weatherData, vehicleType);

        assertEquals(0.5d, result);
    }

    @Test
    void calculateWPEFBikeOrScooterGlazePhenomenonCase() {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 33.0f, "glaze");
        Vehicle vehicleType = Vehicle.BIKE;

        ServiceError thrownException = assertThrows(ServiceError.class, () -> deliveryFeeService.calculateWPEF(weatherData, vehicleType));
        assertEquals("Usage of selected vehicle type is forbidden", thrownException.getMessage());
    }

    @Test
    void calculateWPEFBikeOrScooterHailPhenomenonCase() {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 33.0f, "hail");
        Vehicle vehicleType = Vehicle.SCOOTER;

        ServiceError thrownException = assertThrows(ServiceError.class, () -> deliveryFeeService.calculateWPEF(weatherData, vehicleType));
        assertEquals("Usage of selected vehicle type is forbidden", thrownException.getMessage());
    }

    @Test
    void calculateWPEFBikeOrScooterThunderPhenomenonCase() {
        WeatherDataEntity weatherData = createWeatherDataEntity(15f, 33.0f, "thunder");
        Vehicle vehicleType = Vehicle.BIKE;

        ServiceError thrownException = assertThrows(ServiceError.class, () -> deliveryFeeService.calculateWPEF(weatherData, vehicleType));
        assertEquals("Usage of selected vehicle type is forbidden", thrownException.getMessage());
    }

    @Test
    void calculateWPEFCarVehicleCase() throws ServiceError {
        WeatherDataEntity weatherData = createWeatherDataEntity(-10f, 50f,"thunder");
        Vehicle vehicleType = Vehicle.CAR;

        double result = deliveryFeeService.calculateWPEF(weatherData, vehicleType);

        assertEquals(0d, result);
    }
}
