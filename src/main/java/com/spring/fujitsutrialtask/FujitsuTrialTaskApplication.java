package com.spring.fujitsutrialtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FujitsuTrialTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(FujitsuTrialTaskApplication.class, args);
	}

}
