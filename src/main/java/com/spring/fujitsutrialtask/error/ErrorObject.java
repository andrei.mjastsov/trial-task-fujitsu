package com.spring.fujitsutrialtask.error;

import lombok.Data;

import java.util.Date;

@Data
public class ErrorObject {
    private String message;
    private Date timestamp;
}
