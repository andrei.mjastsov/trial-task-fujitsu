package com.spring.fujitsutrialtask.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handles exceptions and returns an error response.
     *
     * @param ex The exception to be handled.
     * @return A ResponseEntity containing an ErrorObject with details about the error.
     */
    @ExceptionHandler
    public ResponseEntity<ErrorObject> handlerErrors(Exception ex) {

        ErrorObject error = new ErrorObject();
        error.setTimestamp(new Date());
        if (ex instanceof ServiceError) {
            error.setMessage(ex.getMessage());
        } else if (ex instanceof MethodArgumentTypeMismatchException methodArgumentTypeMismatchException) {
            Throwable rootCause = methodArgumentTypeMismatchException.getRootCause();
            if (rootCause != null) {
                error.setMessage("Argument type mismatch - " + rootCause.getMessage());
            } else {
                error.setMessage("Argument type mismatch.");
            }
        } else if (ex instanceof MissingServletRequestParameterException) {
            error.setMessage("Argument is missing.");
        } else {
            error.setMessage("Bad request.");
        }

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
