package com.spring.fujitsutrialtask.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ServiceError extends Exception {

    /**
     * Constructs a new ServiceError with the specified detail message.
     *
     * @param message The detail message (which is saved for later retrieval by the getMessage() method).
     */
    public ServiceError(String message) {
        super(message);
    }
}
