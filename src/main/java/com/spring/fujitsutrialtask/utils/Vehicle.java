package com.spring.fujitsutrialtask.utils;

public enum Vehicle {
    CAR,
    SCOOTER,
    BIKE
}
