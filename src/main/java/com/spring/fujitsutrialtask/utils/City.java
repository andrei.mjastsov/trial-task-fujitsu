package com.spring.fujitsutrialtask.utils;

import lombok.RequiredArgsConstructor;

@SuppressWarnings("squid:S115")
@RequiredArgsConstructor
public enum City {
    TALLINN("Tallinn",4, 3.5, 3),
    TARTU("Tartu", 3.5, 3, 2.5),
    PÄRNU("Pärnu", 3, 2.5, 2);

    private final String asString;
    private final double deliveryFeeForCar;
    private final double deliveryFeeForScooter;
    private final double deliveryFeeForBike;

    /**
     * Retrieves the Road Condition Base Factor (RBF) for a given vehicle type.
     *
     * @param vehicle type of vehicle for which to retrieve the RBF.
     * @return Road Condition Base Factor (RBF) corresponding to the provided vehicle type.
     */
    public double getRBF(Vehicle vehicle) {
        return switch (vehicle) {
            case CAR -> deliveryFeeForCar;
            case SCOOTER -> deliveryFeeForScooter;
            case BIKE -> deliveryFeeForBike;
        };
    }

    @Override
    public String toString() {
        return asString;
    }
}
