package com.spring.fujitsutrialtask.repository;

import com.spring.fujitsutrialtask.entity.WeatherDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface WeatherDataRepository extends JpaRepository<WeatherDataEntity, Integer> {
    WeatherDataEntity getByStationName(String city);

    WeatherDataEntity getByStationNameAndTimestamp(String name, LocalDateTime dateTime);
}
