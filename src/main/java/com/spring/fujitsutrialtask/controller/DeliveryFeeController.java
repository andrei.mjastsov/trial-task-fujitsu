package com.spring.fujitsutrialtask.controller;

import com.spring.fujitsutrialtask.error.ServiceError;
import com.spring.fujitsutrialtask.service.DeliveryFeeService;
import com.spring.fujitsutrialtask.utils.City;
import com.spring.fujitsutrialtask.utils.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/delivery")
public class DeliveryFeeController {

    private final DeliveryFeeService deliveryFeeService;

    /**
     * Calculates the delivery fee for a given city, vehicle type, and timestamp.
     *
     * @param city        The city for which the delivery fee is calculated.
     * @param vehicleType The type of vehicle used for delivery.
     * @param timestamp    (Optional) The timestamp for which the delivery fee is calculated.
     * @return            The calculated delivery fee.
     * @throws ServiceError if weather data does not exist.
     */
    @GetMapping("/fee")
    public ResponseEntity<Double> calculateDeliveryFee(
            @RequestParam City city,
            @RequestParam Vehicle vehicleType,
            @RequestParam(required = false) LocalDateTime timestamp) throws ServiceError {

        double fee = deliveryFeeService.calculateDeliveryFee(city, vehicleType, timestamp);
        return ResponseEntity.ok(fee);
    }

}
