package com.spring.fujitsutrialtask.api.cron;

import com.spring.fujitsutrialtask.mapper.WeatherDataMapper;
import com.spring.fujitsutrialtask.repository.WeatherDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;

@Component
@RequiredArgsConstructor
public class Weather {
    private static final String API_URL = "https://www.ilmateenistus.ee/ilma_andmed/xml/observations.php";
    private final WeatherDataRepository repository;
    private final WeatherDataMapper mapper;

    /**
     * Method get data from cron in xml format, parse it and save into database.
     * @throws IOException if something wrong with URL
     * @throws ParserConfigurationException if DocumentBuilder cannot be created
     * @throws SAXException if any parse error occur
     */
    @Scheduled(cron = "* 15 * * * *")
    public void getWeatherData() throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory dbf;
        dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(new URL(API_URL).openStream());
        repository.saveAll(mapper.convertXmlToEntity(document));
    }
}
