package com.spring.fujitsutrialtask.mapper;

import com.spring.fujitsutrialtask.entity.WeatherDataEntity;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
@Slf4j
@SuppressWarnings("squid:S115")
public abstract class WeatherDataMapper {

    private static final String TALLINN = "Tallinn";
    private static final String TALLINN_HARKU = "Tallinn-Harku";
    private static final String TARTU = "Tartu";
    private static final String TARTU_TORAVERE = "Tartu-Tõravere";
    private static final String PARNU = "Pärnu";


    /**
     * Converts XML document representing weather observations into a collection of WeatherDataEntity objects.
     *
     * @param documentXml XML document containing weather observations.
     * @return WeatherDataEntity objects parsed from the XML document.
     */
    public Iterable<WeatherDataEntity> convertXmlToEntity(Document documentXml) {
        List<WeatherDataEntity> entities = new ArrayList<>();
        NodeList nodeList;
        try {
            nodeList = documentXml.getElementsByTagName("station");
        } catch (NullPointerException e) {
            throw new NullPointerException("xml document was empty.");
        }

        LocalDateTime timestamp;
        try {
            Element observationsElement = (Element) documentXml.getElementsByTagName("observations").item(0);
            String timestampAttribute = observationsElement.getAttribute("timestamp");

            long unixTimestamp = Long.parseLong(timestampAttribute);
            Instant instant = Instant.ofEpochSecond(unixTimestamp);
            timestamp = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            timestamp.format(formatter);
        } catch (Exception ex) {
            log.debug("Timestamp is missing " + ex);
            timestamp = LocalDateTime.now();
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element observationElement;
            observationElement = (Element) nodeList.item(i);
            String name = getContentAsString(observationElement, "name");

            if (name.equals(TALLINN_HARKU) || name.equals(TARTU_TORAVERE) || name.equals(PARNU)) {
                WeatherDataEntity entity = parseObservationElement(observationElement);
                entity.setTimestamp(timestamp);
                entities.add(entity);
            }
        }
        return entities;
    }

    /**
     * Parses an XML element with weather observations into a WeatherDataEntity object.
     *
     * @param observationElement XML element containing weather observations.
     * @return WeatherDataEntity object parsed from the XML element.
     */
    private WeatherDataEntity parseObservationElement(Element observationElement) {
        WeatherDataEntity weatherDataEntity = new WeatherDataEntity();
        try {
            switch (getContentAsString(observationElement, "name")) {
                case TALLINN_HARKU -> weatherDataEntity.setStationName(TALLINN);
                case TARTU_TORAVERE -> weatherDataEntity.setStationName(TARTU);
                case PARNU -> weatherDataEntity.setStationName(PARNU);
                default ->
                    log.debug("Unexpected value: " + getContentAsString(observationElement, "name"));

            }
            weatherDataEntity.setStationWmo(
                    Integer.parseInt(getContentAsString(observationElement, "wmocode")));
            weatherDataEntity.setAirTemperature(
                    Float.parseFloat(getContentAsString(observationElement, "airtemperature")));
            weatherDataEntity.setWindSpeed(
                    Float.parseFloat(getContentAsString(observationElement, "windspeed")));
            weatherDataEntity.setPhenomenon(
                    getContentAsString(observationElement, "phenomenon"));
        } catch (NullPointerException e) {
            log.debug("Weather data is missing: " + e);
        }
        return weatherDataEntity;
    }

    /**
     * Retrieves the text content of the specified tag name from the given XML element.
     *
     * @param element XML element from which to retrieve the content.
     * @param tagName name of the tag whose content is to be retrieved.
     * @return text content of the specified tag name.
     */
    private String getContentAsString(Element element, String tagName) {
        return element.getElementsByTagName(tagName).item(0).getTextContent();
    }

}