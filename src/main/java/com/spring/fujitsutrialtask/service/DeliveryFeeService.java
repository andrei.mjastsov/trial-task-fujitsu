package com.spring.fujitsutrialtask.service;

import com.spring.fujitsutrialtask.entity.WeatherDataEntity;
import com.spring.fujitsutrialtask.error.ServiceError;
import com.spring.fujitsutrialtask.repository.WeatherDataRepository;
import com.spring.fujitsutrialtask.utils.City;
import com.spring.fujitsutrialtask.utils.Vehicle;
import jakarta.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class DeliveryFeeService {

    private final WeatherDataRepository weatherDataRepository;

    /**
     * Calculates the delivery fee for a given city, vehicle type, and timestamp.
     *
     * @param city        The city for which the delivery fee is calculated.
     * @param vehicleType The type of vehicle used for delivery.
     * @param timestamp    (Optional) The timestamp for which the delivery fee is calculated.
     * @return            The calculated delivery fee.
     * @throws ServiceError if weather data does not exist.
     */
    public double calculateDeliveryFee(City city, Vehicle vehicleType, @Nullable LocalDateTime timestamp) throws ServiceError {
        WeatherDataEntity weatherData;
        if (timestamp == null) {
            weatherData = weatherDataRepository.getByStationName(city.toString());
        } else {
            weatherData = weatherDataRepository.getByStationNameAndTimestamp(city.toString(), timestamp);
        }

        if (weatherData == null) {
            throw new ServiceError("Weather data does not exist.");
        }
        double rbf = city.getRBF(vehicleType);
        double atef = calculateATEF(weatherData, vehicleType);
        double wsef = calculateWSEF(weatherData, vehicleType);
        double wpef = calculateWPEF(weatherData, vehicleType);

        return rbf + atef + wsef + wpef;
    }

    /**
     * Calculates the Atmospheric Temperature Effect Factor (ATEF) for a given weather data and vehicle type.
     *
     * @param weatherData weather data entity containing air temperature information.
     * @param vehicleType type of vehicle used for delivery.
     * @return calculated Atmospheric Temperature Effect Factor (ATEF).
     */
    public double calculateATEF(WeatherDataEntity weatherData, Vehicle vehicleType) {
        if (vehicleType == Vehicle.BIKE || vehicleType == Vehicle.SCOOTER) {
            if (weatherData.getAirTemperature() < -10) {
                return  1d;
            } else if (weatherData.getAirTemperature() >= -10d && weatherData.getAirTemperature() <= 0) {
                return 0.5d;
            }
        }
        return 0d;
    }

    /**
     * Calculates the Wind Speed Effect Factor (WSEF) for a given weather data and vehicle type.
     *
     * @param weatherData weather data entity containing wind speed information.
     * @param vehicleType type of vehicle used for delivery.
     * @return calculated Wind Speed Effect Factor (WSEF).
     * @throws ServiceError If the usage of the selected vehicle type is forbidden due to high wind speed.
     */
    public double calculateWSEF(WeatherDataEntity weatherData, Vehicle vehicleType) throws ServiceError {
        if (vehicleType.equals(Vehicle.BIKE)) {
            if (weatherData.getWindSpeed() > 20f) {
                throw new ServiceError("Usage of selected vehicle type is forbidden");
            } else if (10f <= weatherData.getWindSpeed() && weatherData.getWindSpeed() <= 20f) {
                return 0.5d;
            }
        }
        return 0d;
    }

    /**
     * Calculates the Weather Phenomenon Effect Factor (WPEF) for a given weather data and vehicle type.
     *
     * @param weatherData weather data entity containing weather phenomenon information.
     * @param vehicleType type of vehicle used for delivery.
     * @return calculated Weather Phenomenon Effect Factor (WPEF).
     * @throws ServiceError If the usage of the selected vehicle type is forbidden due to adverse weather conditions.
     */
    public double calculateWPEF(WeatherDataEntity weatherData, Vehicle vehicleType) throws ServiceError {
        if (vehicleType == Vehicle.BIKE || vehicleType == Vehicle.SCOOTER) {
            String phenomenon = weatherData.getPhenomenon().toLowerCase();
            if (phenomenon.contains("snow") ||
                    phenomenon.contains("sleet")) {
                return  1d;
            } else if (phenomenon.contains("rain")) {
                return 0.5d;
            } else if (phenomenon.contains("glaze") ||
                        phenomenon.contains("hail") ||
                        phenomenon.contains("thunder")) {
                throw new ServiceError("Usage of selected vehicle type is forbidden");
            }
        }
        return 0d;
    }

}
