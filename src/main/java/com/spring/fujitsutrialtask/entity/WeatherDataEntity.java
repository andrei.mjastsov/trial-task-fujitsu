package com.spring.fujitsutrialtask.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "weather_data")
public class WeatherDataEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "station_name")
    private String stationName;

    @Column(name = "station_wmo")
    private Integer stationWmo;

    @Column(name = "air_temperature")
    private Float airTemperature;

    @Column(name = "wind_speed")
    private Float windSpeed;

    @Column
    private String phenomenon;

    @Column(name = "observation_timestamp", nullable = false)
    private LocalDateTime timestamp;

    /**
     * For testing.
     * @return string of existing entities
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("| %-5d | %-20s | %-10d | %-15.2f | %-10.2f | %-15s | %-30s |",
                id, stationName, stationWmo, airTemperature, windSpeed, phenomenon, timestamp));
        int lineLength = sb.length() - 1;
        sb.append("\n").append("-".repeat(Math.max(0, lineLength))).append("\n");
        return sb.toString();
    }



}
